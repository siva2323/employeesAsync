
const { readInput, groupData, powerPuffData, removeId, sortBasedOnCompany, swap, employeeBirthday } = require("./main");
let input="./data.json";



async function employees(input)
{
        try
        {
            let data=await readInput(input);
            let problem2=await groupData(data);
            let problem3=await powerPuffData(problem2);
            let problem4 =await removeId(problem3);
            let problem5 =await sortBasedOnCompany(problem4);
            let problem6 =await swap(problem5);
            let problem7 =await employeeBirthday(problem6);
            console.log("All the answers are created in answer folder")
        }catch (error)
        {
            console.error("Error occured folder may not exist "+error);
        }
}

employees(input);


